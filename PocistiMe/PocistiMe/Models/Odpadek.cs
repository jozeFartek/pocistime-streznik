﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PocistiMe.Models
{
    public class Odpadek
    {
        public int id { get; set; }
        public String name { get; set; } // Ime lokacije
        public String description { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public int userId { get; set; }
        public String filePath { get; set; }
        public long dateCreated { get; set; } // UNIX timestamp
        public long dateUpdated { get; set; } // UNIX timestamp
        public String status { get; set; }
        public int statusId { get; set; }
        public String locationAddress { get; set; }
        public int volumen { get; set; }

        public bool isLiked { get; set; }

        public List<Tag> tags { get; set; }
        public List<Comment> comments { get; set; }







    }
}
 
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PocistiMe.Models
{
    public class Comment
    {
        public int id { get; set; }
        public int uporabnikId { get; set; }
        public int odpadekId { get; set; }
        public String content { get; set; }
        public long dateCreated { get; set; }
        public String userNickName { get; set; }
        public String userImagePath { get; set; } 
          
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PocistiMe.Models
{
    public class Response
    {
        public int StatusCode { get; set; }
        public String Message { get; set; }
    }
}
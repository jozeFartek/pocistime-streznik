﻿using PocistiMe;
using PocistiMe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace PocistiMe.Controllers
{
    /**
     * iMAGE UPLOAD
     *             https://stackoverflow.com/questions/39953457/how-to-upload-image-file-in-retrofit-2
     */


    public class OdpadekController : ApiController
    {
        // GET: api/Odpadek
        public IEnumerable<Odpadek> Get()
        {
            return OdpadekDatabaseHandler.getOdpadki();
        }

        [HttpPost]
        [Route("api/Odpadek/{id}")]
        // GET: api/Odpadek/5
        public Odpadek Get(int id, Uporabnik uporanik)
        {
            //return DatabaseHandler.getOdpadki().FirstOrDefault(t => t.id == id);
            return OdpadekDatabaseHandler.getOdpadek(id, uporanik.id);
        }

        [Route("api/Odpadek/Liked")]
        [HttpPost]
        public IEnumerable<Odpadek> DeleteLike([FromBody]Uporabnik uporabnik)
        {
            List<Odpadek> odpadki = new List<Odpadek>();
            try
            {
                odpadki = OdpadekDatabaseHandler.getOdpadkiLikes(uporabnik);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            return odpadki;
        }


        [Route("api/Odpadek/Add")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> AddOdpadek()
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {

                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {

                        int MaxContentLength = 1024 * 1024 * 15; //Size = 1 MB  

                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {

                            var message = string.Format("Please Upload image of type .jpg,.gif,.png.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {

                            var message = string.Format("Please Upload a file upto 1 mb.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else
                        {



                            var filePath = HttpContext.Current.Server.MapPath("~/Images/" + postedFile.FileName + extension);

                            postedFile.SaveAs(filePath);

                        }
                    }

                    var message1 = string.Format("Image Updated Successfully.");
                    return Request.CreateErrorResponse(HttpStatusCode.Created, message1); ;
                }
                var res = string.Format("Please Upload a image.");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
            catch (Exception ex)
            {
                var res = string.Format("some Message");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
        }


        /*
      // POST: api/Location
      public void Post([FromBody]string value)
      {
      }

      // PUT: api/Location/5
      public void Put(int id, [FromBody]string value)
      {
      }

      // DELETE: api/Location/5
      public void Delete(int id)
      {
      }
      */
    }
}

﻿using PocistiMe.Database;
using PocistiMe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PocistiMe.Controllers
{
    public class LikeController : ApiController
    {

        [Route("api/Like/Add")]
        [HttpPost]
        public Response AddLike([FromBody]Like like)
        {
            try
            {
                LikeDatabaseHandler.addLike(like);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            return new Response { StatusCode = 200, Message = "OK" };
        }

        [Route("api/Like/Delete")]
        [HttpPost]
        public Response DeleteLike([FromBody]Like like)
        {
            try
            {
                LikeDatabaseHandler.deleteLike(like);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            return new Response { StatusCode = 200, Message = "OK" };
        }
    }
}

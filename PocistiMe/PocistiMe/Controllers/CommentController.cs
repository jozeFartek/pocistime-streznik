﻿using PocistiMe.Database;
using PocistiMe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PocistiMe.Controllers
{
    public class CommentController : ApiController
    {
        [HttpPost]
        public Response AddComent([FromBody]Comment comment)
        {
            CommentDatabaseHandler.addComment(comment);

            return new Response { StatusCode = 200, Message = "OK" };
        }
    }
}

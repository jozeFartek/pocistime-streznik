﻿using PocistiMe.Database;
using PocistiMe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PocistiMe.Controllers
{
    public class UserController : ApiController
    {
        // GET: api/User
        public IEnumerable<Uporabnik> Get()
        {
            return UserDatabaseHandler.getUsers();
        }

        // GET: api/User/5
        public Uporabnik Get(int id)
        {
            return UserDatabaseHandler.getUsers().FirstOrDefault(t => t.id == id);
        }


        [Route("api/User/Login")]
        [HttpPost]
        public Uporabnik Login([FromBody]Uporabnik uporabnik)
        {
            bool iserExist = UserDatabaseHandler.userExist(uporabnik);
            return UserDatabaseHandler.Login(uporabnik);
        }

        [Route("api/User/Register")]
        [HttpPost]
        public Uporabnik Register([FromBody]Uporabnik uporabnik)
        {
            Uporabnik user = new Uporabnik();
            user.id = -1;
            bool userExist = UserDatabaseHandler.userExist(uporabnik);
            if (userExist)
            {
                user.id = -1;
                return user;
            }

            try
            {
                user = UserDatabaseHandler.Register(uporabnik);
            }
            catch (Exception)
            {
            }

             return user;
        }



    }
}

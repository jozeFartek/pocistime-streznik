﻿using PocistiMe.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PocistiMe.Database
{
    public class CommentDatabaseHandler
    {
        public static void addComment(Comment comment)
        {
            using (var conn = new SqlConnection(Properties.Settings.Default.conn))
            {
                DateTime dateTime = DateTime.Now;
                long unixTime = ((DateTimeOffset)dateTime).ToUnixTimeSeconds();

                conn.Open();
                var cmd = new SqlCommand("AddComment", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@OdpadekId", comment.odpadekId);
                    cmd.Parameters.AddWithValue("@UporabnikId", comment.uporabnikId);
                    cmd.Parameters.AddWithValue("@Content", comment.content);
                    cmd.Parameters.AddWithValue("@DateCreated", unixTime);

                var dr = cmd.ExecuteNonQuery();
            }
        }
    }
}
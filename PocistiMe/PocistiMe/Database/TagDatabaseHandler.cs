﻿using PocistiMe.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PocistiMe.Database
{
    public class TagDatabaseHandler
    {
        private static List<Tag> getOdadekTagList(int id)
        {
            List<Tag> tagList = new List<Tag>();
            // GET tag list 
            using (var conn = new SqlConnection(Properties.Settings.Default.conn))
            {
                try
                {
                    conn.Open();
                    var cmd = new SqlCommand("getOdpadekTagById", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    var dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        Tag tag = new Tag
                        {
                            Id = Convert.ToInt32(dr["Id"].ToString()),
                            name = dr["Name"].ToString(),
                        };
                        tagList.Add(tag);
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                }


            }
            return tagList;
        }
    }
}
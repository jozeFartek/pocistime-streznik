﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PocistiMe.Models
{

    // http://www.c-sharpcorner.com/article/uploading-image-to-server-using-web-api-2-0/
    public class OdpadekDatabaseHandler
    {
        // VRNE SEZNAM ODPADKOV
        public static List<Odpadek> getOdpadki()
        {
            List<Odpadek> list = new List<Odpadek>();
            using (var conn = new SqlConnection(Properties.Settings.Default.conn))
            {
                conn.Open();
                var cmd = new SqlCommand("getOdpadki", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //    cmd.Parameters.AddWithValue("ID_Izdelek", id_izdelek);

                var dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Odpadek odpadek = new Odpadek
                    {
                        id = Convert.ToInt32(dr["Id"].ToString()),
                        name = dr["Name"].ToString(),
                        latitude = Convert.ToDouble(dr["Latitude"].ToString()),
                        longitude = Convert.ToDouble(dr["Longitude"].ToString()),
                        userId = Convert.ToInt32(dr["UserID"].ToString()),
                        filePath = dr["Filepath"].ToString(),
                        dateCreated = Convert.ToInt64(dr["DateCreated"].ToString()),
                        dateUpdated = Convert.ToInt64(dr["DateUpdated"].ToString()),
                        status = dr["StatusName"].ToString(),
                        statusId = Convert.ToInt32(dr["StatusId"].ToString()),
                        locationAddress = dr["LocationAddress"].ToString(),
                        description = dr["Description"].ToString(),
                        volumen = Convert.ToInt32(dr["Volumen"].ToString()),
                    };
                    list.Add(odpadek);
                }
                     


            }
            return list;
        }

        public static List<Odpadek> getOdpadkiLikes(Uporabnik uporabnik)
        {
            List<Odpadek> list = new List<Odpadek>();
            using (var conn = new SqlConnection(Properties.Settings.Default.conn))
            {
                conn.Open();
                var cmd = new SqlCommand("getOdpadkiByLikes", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("UporabnikId", uporabnik.id);

                var dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Odpadek odpadek = new Odpadek
                    {
                        id = Convert.ToInt32(dr["Id"].ToString()),
                        name = dr["Name"].ToString(),
                        latitude = Convert.ToDouble(dr["Latitude"].ToString()),
                        longitude = Convert.ToDouble(dr["Longitude"].ToString()),
                        userId = Convert.ToInt32(dr["UserID"].ToString()),
                        filePath = dr["Filepath"].ToString(),
                        dateCreated = Convert.ToInt64(dr["DateCreated"].ToString()),
                        dateUpdated = Convert.ToInt64(dr["DateUpdated"].ToString()),
                        status = dr["StatusName"].ToString(),
                        statusId = Convert.ToInt32(dr["StatusId"].ToString()),
                        locationAddress = dr["LocationAddress"].ToString(),
                        description = dr["Description"].ToString(),
                        volumen = Convert.ToInt32(dr["Volumen"].ToString()),
                    };
                    list.Add(odpadek);
                }



            }
            return list;
        }

        public static Odpadek getOdpadek(int id, int uporabnikId)
        {
            Odpadek odpadek = new Odpadek { id = -1 };

            using (var conn = new SqlConnection(Properties.Settings.Default.conn))
            {
                try
                {
                    conn.Open();
                    var cmd = new SqlCommand("getOdpadekById", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("IdOdpadek", id);

                    var dr = cmd.ExecuteReader();
                    dr.Read();
                    odpadek = new Odpadek
                    {
                        id = Convert.ToInt32(dr["Id"].ToString()),
                        name = dr["Name"].ToString(),
                        latitude = Convert.ToDouble(dr["Latitude"].ToString()),
                        longitude = Convert.ToDouble(dr["Longitude"].ToString()),
                        userId = Convert.ToInt32(dr["UserID"].ToString()),
                        filePath = dr["Filepath"].ToString(),
                        dateCreated = Convert.ToInt64(dr["DateCreated"].ToString()),
                        dateUpdated = Convert.ToInt64(dr["DateUpdated"].ToString()),
                        status = dr["StatusName"].ToString(),
                        statusId = Convert.ToInt32(dr["StatusId"].ToString()),
                        locationAddress = dr["LocationAddress"].ToString(),
                        description = dr["Description"].ToString(),
                        volumen = Convert.ToInt32(dr["Volumen"].ToString()),
                        isLiked = getOdadekIsLiked(uporabnikId, id),
                    };
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                }


            }

            


            odpadek.tags = getOdadekTagList(id);
            odpadek.comments = getOdadekComments(id);
            return odpadek;
        }


        //getIsOdpadekLiked   

        private static bool getOdadekIsLiked(int uporabnikId, int odpadekId)
        {
            bool isLiked = false;
            using (var conn = new SqlConnection(Properties.Settings.Default.conn))
            {
                try
                {
                    conn.Open();
                    var cmd = new SqlCommand("getIsOdpadekLiked", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@OdpadekId", odpadekId);
                    cmd.Parameters.AddWithValue("@UporabnikId", uporabnikId);

                    var dr = cmd.ExecuteReader();
                    int count = 0;
                    while (dr.Read())
                        count++;
                    if (count == 1)
                        isLiked = true;

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                }


            }
            return isLiked;
        }

        private static List<Tag> getOdadekTagList(int id)
        {
            List<Tag> tagList = new List<Tag>();
            // GET tag list 
            using (var conn = new SqlConnection(Properties.Settings.Default.conn))
            {
                try
                {
                    conn.Open();
                    var cmd = new SqlCommand("getOdpadekTagById", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("IdOdpadek", id);

                    var dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        Tag tag = new Tag
                        {
                            Id = Convert.ToInt32(dr["Id"].ToString()),
                            name = dr["Name"].ToString(),
                        };
                        tagList.Add(tag);
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                }


            }
            return tagList;
        }

        private static List<Comment> getOdadekComments(int id)
        {
            List<Comment> comments = new List<Comment>();
            // GET tag list 
            using (var conn = new SqlConnection(Properties.Settings.Default.conn))
            {
                try
                {
                    conn.Open();
                    var cmd = new SqlCommand("getOdpadekCommentsById", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("IdOdpadek", id);

                    var dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        Comment comment = new Comment
                        {
                            id = Convert.ToInt32(dr["Id"].ToString()),
                            uporabnikId = Convert.ToInt32(dr["UporabnikId"].ToString()),
                            odpadekId = Convert.ToInt32(dr["OdpadekId"].ToString()),
                            content = dr["Content"].ToString(),
                            dateCreated = Convert.ToInt64(dr["DateCreated"].ToString()),
                            userNickName = dr["NickName"].ToString(),
                            userImagePath = dr["ImagePath"].ToString(),
                        };
                        comments.Add(comment);
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                }


            }
            return comments;
        }








    }
}

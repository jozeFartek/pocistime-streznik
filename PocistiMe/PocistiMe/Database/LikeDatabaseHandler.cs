﻿using PocistiMe.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PocistiMe.Database
{
    public class LikeDatabaseHandler
    {

        public static void addLike(Like like)
        {
            using (var conn = new SqlConnection(Properties.Settings.Default.conn))
            {
                conn.Open();
                var cmd = new SqlCommand("AddLike", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OdpadekId", like.odpadekId);
                cmd.Parameters.AddWithValue("@UporabnikId", like.uporabnikId);

                var dr = cmd.ExecuteNonQuery();
            }
        }

        public static void deleteLike(Like like)
        {
            using (var conn = new SqlConnection(Properties.Settings.Default.conn))
            {
                conn.Open();
                var cmd = new SqlCommand("DeleteLike", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OdpadekId", like.odpadekId);
                cmd.Parameters.AddWithValue("@UporabnikId", like.uporabnikId);

                var dr = cmd.ExecuteNonQuery();
            }
        }
    }
}
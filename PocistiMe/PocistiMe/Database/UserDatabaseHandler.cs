﻿using PocistiMe.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Http;

namespace PocistiMe.Database
{
    public class UserDatabaseHandler
    {


        // VRNE SEZNAM UPORABNIKOV
        public static List<Uporabnik> getUsers()
        {
            List<Uporabnik> list = new List<Uporabnik>();
            using (var conn = new SqlConnection(Properties.Settings.Default.conn))
            {
                conn.Open();
                var cmd = new SqlCommand("getUsers", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //    cmd.Parameters.AddWithValue("ID_Izdelek", id_izdelek);

                var dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Uporabnik uporabnik = getUserFromSqlDataReader(dr);
                    list.Add(uporabnik);
                }
            }
            return list;
        }

        public static Uporabnik Login(Uporabnik uporabnik)
        {
            Uporabnik userLogin = new Uporabnik();
            userLogin.id = -1;

            using (var conn = new SqlConnection(Properties.Settings.Default.conn))
            {
                String hash = Hash(uporabnik.password);
                conn.Open();
                var cmd = new SqlCommand("userLogin", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@email", uporabnik.email);
                cmd.Parameters.AddWithValue("@password", hash);

                var dr = cmd.ExecuteReader();
                dr.Read();
                userLogin = getUserFromSqlDataReader(dr);
            }
            return userLogin;
        }

        public static Uporabnik Register(Uporabnik uporabnik)
        {
            Uporabnik user = new Uporabnik();
            user.id = -1;

            using (var conn = new SqlConnection(Properties.Settings.Default.conn))
            {
                String hash = Hash(uporabnik.password);
                conn.Open();
                var cmd = new SqlCommand("addUser", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@email", uporabnik.email);
                cmd.Parameters.AddWithValue("@password", hash);
                cmd.Parameters.AddWithValue("@nickName", uporabnik.nickName);
                cmd.Parameters.AddWithValue("@imagePath", "");
                cmd.Parameters.AddWithValue("@dateCreated", getTimestamp());

                var dr = cmd.ExecuteReader();
                dr.Read();
                user = getUserFromSqlDataReader(dr);
            }
            return user;
        }


        public static bool userExist(Uporabnik uporabnik)
        { 
            using (var conn = new SqlConnection(Properties.Settings.Default.conn))
            {
                String hash = Hash(uporabnik.password);
                conn.Open();
                var cmd = new SqlCommand("checkRegister", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@email", uporabnik.email);

                Int32 count = Convert.ToInt32(cmd.ExecuteScalar());
                if (count != 0)
                    return true;
            }
            return false;
        }

        public static Uporabnik getUserFromSqlDataReader(SqlDataReader dr)
        {
            return new Uporabnik
            {
                id = Convert.ToInt32(dr["Id"].ToString()),
                nickName = dr["NickName"].ToString(),
                email = dr["Email"].ToString(),
                password = dr["Password"].ToString(),
                imagePath = dr["ImagePath"].ToString(),
                dateCreated = Convert.ToInt64(dr["DateCreated"].ToString()),
                active = Convert.ToInt32(dr["Active"].ToString()),
            };
        }


        private static string Hash(String text)
        {
            var enc = Encoding.GetEncoding(0);

            byte[] buffer = enc.GetBytes(text);
            var sha1 = SHA1.Create();
            var hash = BitConverter.ToString(sha1.ComputeHash(buffer)).Replace("-", "");
            return hash;
        }

        private static long getTimestamp()
        {
            DateTime dateTime = DateTime.Now;
            long unixTime = ((DateTimeOffset)dateTime).ToUnixTimeSeconds();

            return unixTime;
        }

    }
}